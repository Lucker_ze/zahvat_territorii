#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include <QMainWindow>
#include <QPainter>
#include <QMouseEvent>
#include "ui_gamewindow.h"
#include <QDir>

namespace Ui {
class GameWindow;
}

/*!
 * \brief Класс-виджет, реализующий игровое поле и основной игровой процесс.
 *
 * Данный класс полностью реализует игровой процесс, хранит данные о текущей игре, а также отрисовывает игровой процесс.
 */
class GameWindow : public QMainWindow
{
    Q_OBJECT

 public:
    //!Экземпляр класса, отображающего окно.
    Ui::GameWindow *ui;
    //!Размер игрового поля ШхВ.
    std::pair<int,int> board_size;
    //!Количество игроков.
    int players_count = 0;
    //!Флаг, обозначающий наличие бота в игре.
    bool is_bot;

 private:
    std::vector<std::vector<int>> game_board;
    int curent_player = 0;
    int tp_offset = 0;
    std::vector<bool> skiped_players; //new!
    std::vector<int> skiped_turns; //new!
    std::pair<int,int> dices = std::make_pair(1,1);
    std::vector<QImage> images_set;
    std::vector<QImage> dices_imgs;
    std::vector<QCursor> cursors;
    QImage board41x32_img = QImage(":/resources/board41x32.bmp");
    QImage board20x16_img = QImage(":/resources/board20x16.bmp");
    QImage set_good_img = QImage(":/resources/set_good.bmp");
    QImage set_bad_img = QImage(":/resources/set_bad.bmp");

 protected:
    //!Метод, вызываемый при событиях отрисовки окна.
    virtual void paintEvent(QPaintEvent *ev);
    //!Метод, вызываемый при движении мыши.
    virtual void mouseMoveEvent(QMouseEvent *ev);
    //!Метод, вызываемый при нажатии клавиши.
    virtual void mousePressEvent(QMouseEvent *ev);
 public:
    explicit GameWindow(QWidget *parent = 0);
    ~GameWindow();
    //!Метод для инициализации поля и начала игры.
    void start_window();
    //!Методы для выставления количества игроков.
    void set_players_count(int cnt);
    //!Метод для очистки содержимого игрового поля.
    void clear_all();

 private:
    //!Метод, проверяющий возможность поставить квадрат в место, где находится мышка.
    bool is_good();
    //!Метод, проверяющий возможность поставить квадрат в выбранные координаты.
    //! \param[in] x-координата
    //! \param[in] y-координата
    bool is_good(int cur_x, int cur_y);
    //!Метод, обрабатывающий ход игрока.
    void play_turn();
    //!Метод, реализующий бросок кубиков.
    void throw_dices();
    //!Метод, реализующий ход бота.
    void bot_play_turn();
    //!Метод, обрабатывающий конец хода.
    void turn();
    //!Метод, реализующий конец игры для текущего игрока.
    void end_turns();

 private slots:
    //!Слот для пропуска хода текущим игроком.
    void skip_turn();
    //!Слот, реализующий телепортацию.
    void teleport();

 public: signals:
    //!Сигнал, отправляемый при завершении игры.
    void end_game_signal(std::vector<std::vector<int>> game_board);
};

#endif // GAMEWINDOW_H
