#ifndef WINDOWSCONTROLLER_H
#define WINDOWSCONTROLLER_H

#include "mainwindow.h"
#include "gamewindow.h"
#include "endwindow.h"
#include "ruleswindow.h"

/*!
 * \brief Контроллер окон
 *
 * Класс реализующий управление окнами приложения.
 * Хранит все окна и методы переключения между ними.
 */
class WindowsControllers : public QObject {
     Q_OBJECT
 public:
    explicit WindowsControllers(QWidget *parent = 0);
    ~WindowsControllers() = default;
 public slots:
    //!Слот, инициализирующий и запускающий игровое окно.
    void start_game();
    //!Слот, завершающий игру и выводящий рещультат игры.
    //! \param[in] Игровое поле
    void end_game(std::vector<std::vector<int>> game_board);
    //!Слот, сбрасывающий и перезапускающий игру.
    void restart_game();
    //!Слот, отображающий окно с правилами.
    void show_rules();
 private:
    MainWindow main_window;
    GameWindow game_window;
    endwindow end_window;
    RulesWindow rules_window;
};

#endif // WINDOWSCONTROLLER_H
