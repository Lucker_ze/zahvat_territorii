#include "windowscontroller.h"


void WindowsControllers::start_game(){
    main_window.close();
    //game_window = GameWindow();
    game_window.clear_all();
    auto board_size = main_window.ui->board_size_comboBox->currentText();
    if (board_size == QString("41x32")) {
        //--game_window.ui->board_graphicsView->setGeometry(10,10,820,640);
        game_window.board_size = std::make_pair(41,32);
    } else if(board_size == QString("20x16")){
        //--game_window.ui->board_graphicsView->setGeometry(10,10,400,320);
        game_window.board_size = std::make_pair(20,16);
    } else {
        //--game_window.ui->board_graphicsView->setGeometry(10,10,10,10);
        game_window.board_size = std::make_pair(1,1);
    }
    game_window.is_bot = main_window.ui->is_bot_checkBox->isChecked();
    game_window.set_players_count(main_window.ui->player_counter_spinBox->text().toInt());
    game_window.start_window();
}

void WindowsControllers::end_game(std::vector<std::vector<int>> game_board){
    end_window.start_window(game_board);
    game_window.close();
}
void WindowsControllers::restart_game(){
    end_window.close();
    main_window.show();
}

void WindowsControllers::show_rules(){
    rules_window.show();
}

WindowsControllers::WindowsControllers(QWidget *parent){
    connect(&*main_window.ui->start_pushButton,&QPushButton::clicked, this, &WindowsControllers::start_game);
    connect(&game_window, &GameWindow::end_game_signal, this,&WindowsControllers::end_game);
    connect(&*end_window.ui->restart_pushButton, &QPushButton::clicked, this, &WindowsControllers::restart_game);
    connect(&*game_window.ui->rules_pushButton, &QPushButton::clicked, this, &WindowsControllers::show_rules);
    main_window.show();
}
