#include "endwindow.h"
#include "ui_endwindow.h"

endwindow::endwindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::endwindow)
{
    ui->setupUi(this);
}

endwindow::~endwindow()
{
    delete ui;
}

void endwindow::start_window(std::vector<std::vector<int>> game_board){
    std::vector<std::pair<int,int>> players_cnt(5,std::make_pair(0,0));
    for(int i =0; i < players_cnt.size(); i++)
        players_cnt[i].second = i+1;
    for(int i =0; i < game_board.size(); i++){
        for(int j =0; j < game_board[i].size(); j++){
            if(game_board[i][j]>0)
                players_cnt[game_board[i][j]-1].first++;
        }
    }
    std::sort(players_cnt.rbegin(),players_cnt.rend());
    ui->player1_label->setText("Игрок " + QString::number(players_cnt[0].second) + ": " + QString::number(players_cnt[0].first));
    ui->player2_label->setText("Игрок " + QString::number(players_cnt[1].second) + ": " + QString::number(players_cnt[1].first));
    ui->player3_label->setText("Игрок " + QString::number(players_cnt[2].second) + ": " + QString::number(players_cnt[2].first));
    ui->player4_label->setText("Игрок " + QString::number(players_cnt[3].second) + ": " + QString::number(players_cnt[3].first));
    ui->player5_label->setText("Игрок " + QString::number(players_cnt[4].second) + ": " + QString::number(players_cnt[4].first));
    repaint();
    this->show();
}
