#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->board_size_comboBox->addItem("41x32");
    ui->board_size_comboBox->addItem("20x16");
}

MainWindow::~MainWindow()
{
    delete ui;
}
