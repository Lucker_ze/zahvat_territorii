#include "gamewindow.h"
#include <QDebug>

GameWindow::GameWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GameWindow)
    //painter(this)
{
    ui->setupUi(this);
    this->setMouseTracking(true);
    centralWidget()->setMouseTracking(true);
    curent_player = 1;
    images_set.push_back(QImage(":/resources/pl1_set.bmp"));
    images_set.push_back(QImage(":/resources/pl2_set.bmp"));
    images_set.push_back(QImage(":/resources/pl3_set.bmp"));
    images_set.push_back(QImage(":/resources/pl4_set.bmp"));
    images_set.push_back(QImage(":/resources/pl5_set.bmp"));
    dices_imgs.push_back(QImage(":/resources/dice_1.png"));
    dices_imgs.push_back(QImage(":/resources/dice_2.png"));
    dices_imgs.push_back(QImage(":/resources/dice_3.png"));
    dices_imgs.push_back(QImage(":/resources/dice_4.png"));
    dices_imgs.push_back(QImage(":/resources/dice_5.png"));
    dices_imgs.push_back(QImage(":/resources/dice_6.png"));
    cursors.push_back(QCursor(QPixmap(":/resources/cursor_white_32.png")));
    cursors.push_back(QCursor(QPixmap(":/resources/cursor_p1_32.png")));
    cursors.push_back(QCursor(QPixmap(":/resources/cursor_p2_32.png")));
    cursors.push_back(QCursor(QPixmap(":/resources/cursor_p3_32.png")));
    cursors.push_back(QCursor(QPixmap(":/resources/cursor_p4_32.png")));
    cursors.push_back(QCursor(QPixmap(":/resources/cursor_p5_32.png")));

    connect(ui->skip_pushButton,&QPushButton::clicked,this,&GameWindow::skip_turn);
    connect(ui->teleport_pushButton,&QPushButton::clicked,this,&GameWindow::teleport);
    connect(ui->end_pushButton,&QPushButton::clicked,this,&GameWindow::end_turns);
}

GameWindow::~GameWindow()
{
    delete ui;
}

void GameWindow::clear_all(){
    //board_size = std::pair<int,int>();
    //players_count = 0;
    //is_bot = false;

    game_board = std::vector<std::vector<int>>();
    //curent_player = 0;
    skiped_players = std::vector<bool>(); //new!
    skiped_turns = std::vector<int>(); //new!
    //dices = std::make_pair(1,1);
}

void GameWindow::start_window(){
    game_board.resize(board_size.first, std::vector<int>(board_size.second, 0));
    //game_board[0][0] = 1;
    //game_board[0].back() = 2;
    //game_board.back()[0] = 3;
    //game_board.back().back() = 4;
    curent_player = 1;
    int perimetr = board_size.first*2 + board_size.second*2;
    int shag = perimetr/players_count;
    int cur_point = std::rand()%perimetr;
    for(int i = 0; i < players_count; i++){
        if(cur_point < board_size.first) {
            game_board[cur_point][0] = i+1;
        } else {
            if(cur_point < board_size.first + board_size.second) {
                game_board.back()[cur_point-board_size.first] = i+1;
            } else {
                if (cur_point < 2*board_size.first + board_size.second) {
                    game_board[game_board.size() - (cur_point - board_size.first - board_size.second)].back() = i+1;
                } else {
                    game_board[0][cur_point - 2*board_size.first - board_size.second] = i+1;
                }
            }
        }
        cur_point += shag;
        cur_point %= perimetr;
    }

    throw_dices();
    setCursor(cursors[curent_player]);
    repaint();
    this->show();
}

void GameWindow::set_players_count(int cnt){
    players_count = cnt;
    skiped_players.resize(cnt,0);
    skiped_turns.resize(cnt,0);
    if(is_bot)
        skiped_players.back() = true;
}

void GameWindow::paintEvent(QPaintEvent *ev){
    int x_pose = this->mapFromGlobal(QCursor::pos()).x();
    int y_pose = this->mapFromGlobal(QCursor::pos()).y();
    QPainter painter(this);
    if(board_size == std::make_pair(41,32))
        painter.drawImage(10,10,board41x32_img);
    else if (board_size == std::make_pair(20,16))
        painter.drawImage(10,10,board20x16_img);

    for(int i = 0; i < board_size.first; i++){
        for(int j = 0; j < board_size.second; j++){
            if(game_board[i][j])
                painter.drawImage(i*20+10,j*20+10,images_set[game_board[i][j]-1]);
        }
    }
    int offset_x = std::min(0,(board_size.first+1)*20 - (x_pose+dices.first*20));
    int offset_y = std::min(0,(board_size.second+1)*20 - (y_pose+dices.second*20));
    if(is_good()){
        for(int i = 0; i < dices.first; i++){
            for(int j = 0; j < dices.second; j++){
                painter.drawImage(std::min(std::max(x_pose-10 + offset_x,10),board_size.first*20-10) + i*20,
                                  std::min(std::max(y_pose-10 + offset_y,10),board_size.second*20-10) + j*20,
                                  set_good_img);
             }
         }
    } else {
        for(int i = 0; i < dices.first; i++){
            for(int j = 0; j < dices.second; j++){
                painter.drawImage(std::min(std::max(x_pose-10 + offset_x,10),board_size.first*20-10) + i*20,
                                  std::min(std::max(y_pose-10 + offset_y,10),board_size.second*20-10) + j*20,
                                  set_bad_img);
             }
         }
    }

    painter.drawImage(900,10,dices_imgs[dices.first-1]);
    painter.drawImage(950,10,dices_imgs[dices.second-1]);
}

void GameWindow::mouseMoveEvent(QMouseEvent *ev) {
 repaint();
 //qDebug() <<this->mapFromGlobal(QCursor::pos()).x() << " " << this->mapFromGlobal(QCursor::pos()).y();
}

void GameWindow::mousePressEvent(QMouseEvent *ev){
    if(ev->buttons() == Qt::LeftButton){
        if(is_good()){
            play_turn();
            throw_dices();
            repaint();
        }
    }
    if(ev->buttons() == Qt::RightButton){
        std::swap(dices.first,dices.second);
        repaint();
    }
}

bool GameWindow::is_good(int cur_x, int cur_y){
    if(cur_x + dices.first > board_size.first || cur_y + dices.second > board_size.second)
        return false;

    bool ok = true;
    for(int i = cur_x; i < cur_x + dices.first; i++){
        for(int j = cur_y; j < cur_y + dices.second; j++){
            if(game_board[i][j] != 0)
                ok = false;
        }
    }
    if(ok){
        ok = false;
        if(cur_x-1 >= 0)
            for(int i = cur_y; i < cur_y + dices.second; i++)
                if(game_board[cur_x-1][i] == curent_player)
                    ok = true;
        if(cur_y+dices.second < board_size.second)
            for(int i = cur_x; i < cur_x + dices.first; i++)
                if(game_board[i][cur_y+dices.second] == curent_player)
                    ok = true;
        if(cur_x+dices.first < board_size.first)
            for(int i = cur_y; i < cur_y + dices.second; i++)
                if(game_board[cur_x+dices.first][i] == curent_player)
                    ok = true;
        if(cur_y-1 >= 0)
            for(int i = cur_x; i < cur_x + dices.first; i++)
                if(game_board[i][cur_y-1] == curent_player)
                    ok = true;

        if(cur_x-1-tp_offset >= 0)
            for(int i = cur_y; i < cur_y + dices.second; i++)
                if(game_board[cur_x-1-tp_offset][i] == curent_player)
                    ok = true;
        if(cur_y+dices.second+tp_offset < board_size.second)
            for(int i = cur_x; i < cur_x + dices.first; i++)
                if(game_board[i][cur_y+dices.second+tp_offset] == curent_player)
                    ok = true;
        if(cur_x+dices.first+tp_offset < board_size.first)
            for(int i = cur_y; i < cur_y + dices.second; i++)
                if(game_board[cur_x+dices.first+tp_offset][i] == curent_player)
                    ok = true;
        if(cur_y-1-tp_offset >= 0)
            for(int i = cur_x; i < cur_x + dices.first; i++)
                if(game_board[i][cur_y-1-tp_offset] == curent_player)
                    ok = true;
        if(ok)
            return true;
    }
    return false;
}

bool GameWindow::is_good(){
    int x = this->mapFromGlobal(QCursor::pos()).x();
    int y = this->mapFromGlobal(QCursor::pos()).y();
    int offset_x = std::min(0,(board_size.first+1)*20 - (x+dices.first*20));
    int offset_y = std::min(0,(board_size.second+1)*20 - (y+dices.second*20));
    x += offset_x;
    y += offset_y;
    if(x >= 10 && x <= board_size.first*20 &&
            y >= 10 && y <= board_size.second*20){
        int cur_x = (x-10)/20;
        int cur_y = (y-10)/20;
        return is_good(cur_x, cur_y);
    }
}

void GameWindow::skip_turn(){
    skiped_turns[curent_player-1]++;
    if(skiped_turns[curent_player-1] > 4)
        skiped_players[curent_player-1] = true;
    turn();
}

void GameWindow::end_turns(){
    skiped_players[curent_player-1] = true;
    turn();
}

void GameWindow::turn(){
    tp_offset = 0;
    curent_player %= players_count;
    curent_player++;
    throw_dices();
    repaint();
    if(is_bot && curent_player == players_count){
        bot_play_turn();
        curent_player %= players_count;
        curent_player++;
        throw_dices();
        repaint();
    }
    bool all_skiped = true;
    for(int i = 0; i < skiped_players.size();i++)
        all_skiped &= skiped_players[i];
    if(all_skiped){
        emit end_game_signal(game_board);
    }
    if(skiped_players[curent_player-1] && !all_skiped)
        turn();
    setCursor(cursors[curent_player]);
}

void GameWindow::teleport(){
    if(dices.first == dices.second && dices.first != 1){
        tp_offset = dices.first;
        dices.first = 1;
        dices.second = 1;
        repaint();
    }
}

void GameWindow::play_turn(){
    int x = this->mapFromGlobal(QCursor::pos()).x();
    int y = this->mapFromGlobal(QCursor::pos()).y();
    int offset_x = std::min(0,(board_size.first+1)*20 - (x+dices.first*20));
    int offset_y = std::min(0,(board_size.second+1)*20 - (y+dices.second*20));
    x += offset_x;
    y += offset_y;
    int cur_x = (x-10)/20;
    int cur_y = (y-10)/20;
    for(int i = 0; i < dices.first; i++)
        for(int j = 0; j < dices.second; j++)
            game_board[cur_x+i][cur_y+j] = curent_player;
    turn();
}

void GameWindow::bot_play_turn(){
    std::vector<std::pair<int,int>> good_poses;
    for(int i = 0; i < board_size.first; i++){
        for(int j =0; j < board_size.second; j++){
            if(is_good(i,j)){
                good_poses.push_back(std::make_pair(i,j));
            }
        }
    }
    if(good_poses.size() > 0){
        int ind = std::rand()%good_poses.size();

        for(int i = 0; i < dices.first; i++)
            for(int j = 0; j < dices.second; j++)
                game_board[good_poses[ind].first+i][good_poses[ind].second+j] = curent_player;
    }
}

void GameWindow::throw_dices(){
    dices.first = std::rand()%6+1;
    dices.second = std::rand()%6+1;
}
