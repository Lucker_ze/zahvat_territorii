#ifndef ENDWINDOW_H
#define ENDWINDOW_H

#include <QWidget>
#include "ui_endwindow.h"

namespace Ui {
class endwindow;
}

/*!
 * \brief Класс-виджет, в котором реализовано меню конца игры.
 */
class endwindow : public QWidget
{
    Q_OBJECT

public:
    explicit endwindow(QWidget *parent = 0);
    ~endwindow();

    //!Метод для подсчета очков и инициализации окна.
    //! \param[in] Игровое поле.
    void start_window(std::vector<std::vector<int>> game_board);

    Ui::endwindow *ui;
};

#endif // ENDWINDOW_H
