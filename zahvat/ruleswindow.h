#ifndef RULESWINDOW_H
#define RULESWINDOW_H

#include <QMainWindow>
#include "ui_ruleswindow.h"

namespace Ui {
class RulesWindow;
}

/*!
 * \brief Класс-виджет, в котором реализовано отображение правил игры.
 */
class RulesWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit RulesWindow(QWidget *parent = nullptr);
    ~RulesWindow();

    Ui::RulesWindow *ui;
};

#endif // RULESWINDOW_H
